# Bulk project update

Bulk update a number of projects or all projects in a group. Write a report of what changes.

## Features

- Bulk updates all projects in a group or alternatively, all specified projects in config.yml
- Update attributes listed here: https://docs.gitlab.com/ee/api/projects.html#edit-project
- Produces a CSV of changes

## Usage

`python3 bulk_edit_projects.py $GIT_TOKEN $CONFIG_YML`

## Configuration

- edit config.yml
  - specify GitLab URL and groups or projects using their ID.
  - If groups are used, all projects in the group are retrieved instead of individual projects.
  - specify what attributes to update. Attribute key as in this list https://docs.gitlab.com/ee/api/projects.html#edit-project. Attribute value will be the value the attribute is set to.
- A CSV file will be written that lists all edited projects, the attribute as it was before and what it has been set to.

## DISCLAIMER

This script is provided **as is**. I take no responsibility for any consequences of your usage of this script. I offer no further support. Changing project attributes in bulk means you can mess up a whole lot of configuration with it that will take a lot of time to roll back, potentially blocking your devs in their productive activity. **DO NOT** use this if you are not sure what you are doing.



