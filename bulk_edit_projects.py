#!/usr/bin/env python3

import gitlab
import json
import csv
import os
import argparse
import requests
import re
import urllib.parse
import time
import yaml

def write_csv(project_objects, attributes):
    file_name = "projects_report.csv"
    with open(file_name,"w") as report:
        reportwriter = csv.writer(report, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        project_data = ["id","name","path_with_namespace"]
        for key in attributes.keys():
            project_data.append(key + "_before")
            project_data.append(key + "_after")
        reportwriter.writerow(project_data)
        for project in project_objects:
            row = []
            for attribute in project_data:
                if "before" in attribute:
                    field = attribute[:attribute.rfind("_")]
                    row.append(project[field])
                elif "after" in attribute:
                    field = attribute[:attribute.rfind("_")]
                    row.append(attributes[field])
                else:
                    row.append(project[attribute])
            reportwriter.writerow(row)

def change_project_settings(gl, projects, attributes, token):
    project_objects = []
    headers = {'PRIVATE-TOKEN': token}
    payload = attributes
    for project in projects:
        project_object = gl.projects.get(project)
        update = requests.put("%s/projects/%s" % (gl.api_url, project_object.attributes["id"]), headers=headers, params=payload)
        if update.status_code == 200:
            project_objects.append(project_object.attributes)
    return project_objects

def get_group_projects(gl, groups):
    projects = []
    for topgroup in groups:
        group = gl.groups.get(topgroup)
        group_projects = group.projects.list(as_list=False, include_subgroups=True)
        for group_project in group_projects:
            projects.append(group_project.attributes["id"])
    return projects

parser = argparse.ArgumentParser(description='Create report for GitLab issues')
parser.add_argument('token', help='API token able to read the requested projects')
parser.add_argument('configfile', help='CSV file that defines requested projects')
args = parser.parse_args()

gitlaburl = "https://gitlab.com/"
gl = gitlab.Gitlab(gitlaburl, private_token=args.token)

configfile = args.configfile
groups = []
projects = []
attributes = {}

with open(configfile, "r") as c:
    config = yaml.load(c, Loader=yaml.FullLoader)
    gitlaburl = config["gitlab_url"] if config["gitlab_url"].endswith("/") else config["gitlab_url"] + "/"
    gl = gitlab.Gitlab(gitlaburl, private_token=args.token)
    if "groups" in config:
        groups = config["groups"]
        projects = get_group_projects(gl, groups)
    else:
        if not "projects" in config:
            print("Error: No group or project configured. Stopping.")
            exit(1)
        else:
            projects = config["projects"]

    if "attributes" in config:
        attributes = config["attributes"]
    else:
        print("No attributes to edit provided, exiting")

project_objects = change_project_settings(gl, projects, attributes, args.token)
write_csv(project_objects, attributes)
